import React, {Component} from 'react';
import {fetchOneNews} from "../../store/actions/news";
import {deleteComment, fetchComments} from "../../store/actions/comments";
import {connect} from "react-redux";
import CommentForm from "../../components/CommentForm/CommentForm";

class OneNews extends Component {
    componentDidMount() {
        this.props.onFetchOneNews(this.props.match.params.id);
        this.props.onFetchComments(this.props.match.params.id);

    }
    render() {
        console.log(this.props.comments);
        return (
            <div>
                <h1>{this.props.news[0] && this.props.news[0].title}</h1>
                <p>{this.props.news[0] && this.props.news[0].publication_date}</p>
                <p>{this.props.news[0] && this.props.news[0].content}</p>
                <h2>Comments</h2>
                {this.props.comments.map((one,key)=>{
                    return <div key={key}>
                        <p><b style={{paddingRight: '50px'}}>{one.author}</b>wrote : {one.comment} <button onClick={()=>this.props.deleteComment(one.id)}>Delete</button></p>
                    </div>
                })}
                <h2>Add comment</h2>
                <CommentForm/>

            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        news: state.new.news,
        comments: state.comm.comments
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchOneNews: id => dispatch(fetchOneNews(id)),
        onFetchComments: (id) => dispatch(fetchComments(id)),
        deleteComment: (commentData) => dispatch(deleteComment(commentData))

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(OneNews);

