import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody} from "reactstrap";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import NewsThumbnail from "../../components/NewsThumbnail/NewsThumbnail";
import {deleteNews, fetchNews} from "../../store/actions/news";
import {fetchComments} from "../../store/actions/comments";

class News extends Component {

    componentDidMount() {
        this.props.onFetchNews();
    }

    render() {
        return (
            <Fragment>
                <h2>
                    Posts
                    <Link to='/news/new'>
                        <Button
                            color="primary"
                            className="float-right"
                        >
                            Add new post
                        </Button>
                    </Link>
                </h2>

                {this.props.news.map((oneNews,key) => (
                    <Card key={oneNews.id}>
                        <CardBody key={key}>
                            <NewsThumbnail image={oneNews.image}/>

                            <strong style={{marginLeft: '10px'}}>
                                {oneNews.content}
                            </strong>
                            <p>{oneNews.publication_date}</p>
                            <Link to={"/news/" + oneNews.id}>
                                Read Full Post >>
                            </Link>
                            <button onClick={()=>this.props.deleteNews(oneNews.id)}>Delete</button>


                        </CardBody>
                    </Card>
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        news: state.new.news,
        comments: state.comm.comments
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchNews: () => dispatch(fetchNews()),
        onFetchComments: () => dispatch(fetchComments()),
        deleteNews: (newsData) => dispatch(deleteNews(newsData))

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(News);
