import React, {Component, Fragment} from 'react';
import NewsForm from "../../components/NewsForm/NewsForm";
import {connect} from "react-redux";
import {createNews} from "../../store/actions/news";

class NewNews extends Component {

    createNews = NewsData => {
        this.props.onNewsCreated(NewsData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>Add news</h2>
                <NewsForm onSubmit={this.createNews} />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onNewsCreated: NewsData => dispatch(createNews(NewsData))
    }
};



export default connect(null, mapDispatchToProps)(NewNews);

