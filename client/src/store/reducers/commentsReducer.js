import {FETCH_COMMENTS_SUCCESS} from "../actions/actionTypes";

const initialState = {
    comments: []
};

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_SUCCESS:
            console.log("in reducer", action.comments);
            return {...state, comments: action.comments};
        default:
            return state;
    }
};

export default newsReducer;