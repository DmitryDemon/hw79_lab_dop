const express = require('express');
const nanoid = require('nanoid');

const createRouter = connection => {
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `categories`', (error,results) =>{
            if (error){
                res.status(500).send({error:'Database error'});
            }
            res.send(results);
        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `categories` WHERE `id` = ?',req.params.id, (error,results) =>{
            console.log(results);
            if (error){
                res.status(500).send({error:'Database error'});
            }

            if (results[0]){
                res.send(results[0]);
            } else {
                res.status(404).send({error:'Item not found'});
            }

        });
    });

    router.post('/', (req, res) => {
        const category = req.body;
        connection.query('INSERT INTO `categories` (`title`, `description`) VALUES (?, ?)',
        [category.title, category.description],
        (error, results) => {
          if (error){
            console.log(error);
            res.status(500).send({error:'Database error'});
          }

          res.send({message: 'OK'});
        }
        );
    });

  router.delete('/:id', (req, res) => {//функция не прописана
    connection.query('DELETE FROM `categories` WHERE `id` = ?',req.params.id, (error,results) =>{
      if (error){
        res.status(500).send({error:'Database error'});
      }
      res.send({message: 'OK'});
    });
  });

  router.put('/:id', (req, res) => {
        const category = req.body;

        let qery = 'UPDATE `categories` SET `title` = ?, `description` = ? WHERE `id` = ?';
        const val = [category.title, category.description, req.params.id];

        connection.query(qery,val,
            (error, results) => {
                if (error){
                    console.log(error);
                    res.status(500).send({error:'Database error'});
                }

                res.send({message: 'OK'});
            }
        );
    });

    return router;
};




module.exports = createRouter;